<?php
//creating tesint database for unit test
$db = new \SQLite3('./UnitTesting/testingdb');
//create address table
$statement  = $db->prepare("CREATE TABLE IF NOT EXISTS address(id INTEGER PRIMARY KEY AUTOINCREMENT,name CHAR(50) NOT NULL ,phone integer NOT NULL, street CHAR(50) NOT NULL);");
$statement->execute();

//insert some values
$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Michal', '506088156', 'Michalowskiego 41');");
$statement->execute();
$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Marcin', '502145785', 'Opata Rybickiego 1');");
$statement->execute();
$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Piotr', '504212369', 'Horacego 23');");
$statement->execute();
$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Albert', '605458547', 'Jan Pawła 67');");
$statement->execute();



//creating database for application
$db = new \SQLite3('./application/database/localdb');
//create address table
$statement  = $db->prepare("CREATE TABLE IF NOT EXISTS address(id INTEGER PRIMARY KEY AUTOINCREMENT,name CHAR(50) NOT NULL ,phone integer NOT NULL, street CHAR(50) NOT NULL);");
$statement->execute();

//insert some values
$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Michal', '506088156', 'Michalowskiego 41');");
$statement->execute();

$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Marcin', '502145785', 'Opata Rybickiego 1');");
$statement->execute();

$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Piotr', '504212369', 'Horacego 23');");
$statement->execute();

$statement  = $db->prepare("INSERT INTO address (name, phone, street) VALUES ('Albert', '605458547', 'Jan Pawła 67');");
$statement->execute();
