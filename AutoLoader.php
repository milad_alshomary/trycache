<?php
/**
 * Class AutoLoader
 * @author Milad Alshomary
 * AutoLoader class is used to do the autoloading functionality
 * based on the PSR-0 standards
 */

class AutoLoader{

	/*
	* register is a static method to be called 
	* when we want to append the AutoLoader functionality
	*/
	public static function register() {
		spl_autoload_register(array(AutoLoader::class, 'load_class'));
	}

	/*
	* Whenever a class is mentioned in the code, this 
	* method will be called to do the auto loading.
	*/
	public static function load_class($class_name) {
		$class_name = ltrim($class_name, '\\');
		$file_name  = '';
		$namespace = '';
		if ($last_ns_pos = strripos($class_name, '\\')) {
			$namespace  = substr($class_name, 0, $last_ns_pos);
			$class_name = substr($class_name, $last_ns_pos + 1);
			$file_name  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
		}
		$file_name .= str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';
		require $file_name;
	}
}