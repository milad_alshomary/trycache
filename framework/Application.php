<?php
namespace framework;

use framework\UrlDispacher;
use framework\Request;
use framework\datasource\DataSourceFactory;
use framework\HttpException;
use framework\Response;

/**
 * Class Application
 * @author Milad Alshomary
 * Application class is the main entry of the web application
 */
class Application {
	/**
	* @var array of configuration
	*/
	private $config;

	/**
	* @var Dispacher, the object that does the whole routing thing,
	* finding the proper controller/action to be called based on the required url
	*/
	private $dispacher;


	/**
	* @var Request. an object that holds all information
	* required about the http request
	*/
	public $request;


	/**
	* @var Response. an object that does the logic
	* of rendering the response content
	*/
	public $response;


	/**
	* @var DataSource.
	*/
	public $datasource;

	/**
	* @var Application
	*/
	private static $instance;

	/**
	* initializing an instance of the application
	*/
	public function init($config) {
		self::$instance = new Application($config);
		return self::$instance;
	}

	/**
	* return an instance of the application that was 
	* already initialized in init function
	* it also can be used to initialize the instance
	*/
	public function getInstance($config=[]){
		if(self::$instance == null) {
			self::$instance = new Application($config);
		}
		return self::$instance;
	}

	/**
	* Constructor.
	* @param array $config name-value pairs to configure the application framework
	*/
	private function __construct($config = []) {
		try {

			$this->config = $config;
			//create the request object
			$this->request = new Request();

			//create the request object
			$this->response = new Response($config['response_type']);

			//initializing the datasource based on the config. 
			//users can choose what data source to choose.
			$this->initDataSource($config);

			//initialize the dispacher component
			$routes = isset($this->config['routes']) ? $this->config['routes'] : [];
			$this->dispacher = new UrlDispacher($routes);

		} catch(\HttpException $ex) {
			$this->response->renderException($ex);
		}
	}

	/**
	* The main entry to the application. it reads the url and does
	* the dispaching to find the requested controller/action and execute it
	*/
	public function run(){
		try {
			//it fills Application::$params with post/get prarms passed by the request
			//and returns the requested controller/action
			$route = $this->dispacher->parseRequest($this->request);

			//get instance of the controller
			//run the action and return the results
			$result = $this->getCallableAction($route);

			$this->response->render($result);

		} catch(HttpException $ex) {
			$this->response->renderException($ex);
		}
	}

	private function initDataSource($config) {
		$this->datasource = DataSourceFactory::getDatasource($config['datasource']);
	}

	private function getCallableAction($route){
		list($controller, $action) = $route;
		//capitalize the first letter of the requested controller
		$controller = ucwords($controller);
		$controller = 'application\controllers\\' . $controller. 'Controller';
		$class  = new \ReflectionClass($controller);
		$object = $class->newInstanceArgs([]);
		if (method_exists($object, $action)) {
			$method = new \ReflectionMethod($object, $action);
			//TODO , verify if the class extends controller class
			//run before action on the controller.
			$object->beforeAction();
			//run the specified action
			$result = $method->invoke($object);
			//run after action on the controller
			$object->afterAction();
			
			return $result;
		}

		//if we couldn't find the proper controller/action then we throw exception
		throw new HttpException(400, "error in parsing request");
	}

}