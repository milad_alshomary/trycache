<?php
namespace framework;

use framework\HttpException;

class UrlDispacher {
	/**
	* @var array of routes
	*/
	private $routes;


	/**
	* Constructor.
	* @param array $routes name-value pairs url string and the maching controller/action
	* to be called
	*/
	public function __construct($routes = []) {
		$this->routes = $routes;
	}

	/**
	* @var Request class. This function will match the request
	* to the proper route. if none mached then it will throw
	* not found exception
	*/
	public function parseRequest($request) {
		foreach ($this->routes as $key => $value) {
			if($this->isRouteMaching($request, $key)) {
				$route = explode('/', $value);
				return $route;
			}
		}

		//if none of the routes mache, then we throw not found
		//exception
		throw new HttpException(404, "requested uri is not found");
	}

	/**
	* @var $request Request object that express the http request
	* @var $route string, represent the route we are comparing
	*/
	private function isRouteMaching($request, $route){
		//TODO accept in the url route more than one space between the method and the path
		list($method, $path) = explode(' ', $route);
		if($path == $request->path && $method == $request->http_method){
			return true;
		} else {
			return false;
		}
	}
}