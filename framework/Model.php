<?php
namespace framework;

use framework\Application;

/**
 * Class Model
 * @author Milad Alshomary
 * Model class is used to abstract all the logic related to model layer functionality
 */
abstract class Model {

	private $attributes = [];

	/**
	* @return string that represent the name of the source
	* this model is map to. For example in sql model it is supposed to return 
	* table name.
	*/
	public abstract static function sourceName();

	/**
	* @var $id Integer the id of model to be loaded
	* @return one model or null
	*/
	public static function findOne($id) {
		//get the datasource from Application class and call find on it
		$model = Application::getInstance()->datasource->findOne(get_called_class(), $id);
		return $model;
	}

	/**
	* @var $limit number of results we need to retrieve of this model
	* @var $offset at which offset we need to start retrieveing
	* @return array of models
	*/
	public static function find($criteria=[]) {
		//get the datasource from Application class and call find on it
		$models = Application::getInstance()->datasource->find(get_called_class(), $criteria);
		return $models;
	}

	/**
	* save the object into the data source
	*/
	public function save() {
		//if validation failed, don't save the model
		if(!$this->validateModel()){
			return false;
		}

		//check if the model new or updated
		//TODO use more elegant way to check if model is new or updated
		if(isset($this->attributes['id']) && $this->attributes['id'] > 0){
			//update
			return Application::getInstance()->datasource->update($this->attributes['id'], $this);
		} else {
			//new
			Application::getInstance()->datasource->insert($this);
			//get last inserted id from
			$last_id = Application::getInstance()->datasource->getLastInsertedId();
			$this->attributes['id'] = $last_id;
			$this->id = $last_id;
			return true;
		}
	}

	/**
	* delete the object from the data soruce
	*/
	public function removeBy($by) {
		return Application::getInstance()->datasource->deleteBy($by, $this);
	}

	public function setAttributes($attributes){
		$this->attributes = array_merge($this->attributes, $attributes);
		foreach ($attributes as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getAttributes(){
		if($this->attributes == null) {
			$this->attributes = get_object_vars($this);
		}

		return $this->attributes;
	}

	/**
	* This function will be called right before save is
	* applied to do farther validation on the saved model
	*/
	public function validateModel() {
		return true;
	}

	/**
	* PHP magic function. we will use it 
	*/
	public function __set($name, $value) {
		$this->$name = $value;
		$this->attributes[$name] = $value;
	}

}