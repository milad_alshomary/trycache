<?php
namespace framework;

/**
 * Class Controller
 * @author Milad Alshomary
 * Controller class is used to abstract all the logic related to controller layer functionality
 */
class Controller {

	//TODO supporting params passing to the view to make it 
	// more dynamic.
	public function fetchView($view_name) {
		//load the html view from views folder and return its content
		$file_name = Application::getInstance()->request->app_path . '/views/'. $view_name . '.php';
		if(file_exists($file_name)){
			ob_start();
			ob_implicit_flush(false);
			require($file_name);
			$html = ob_get_clean();
			return $html;
		} else {
			throw new HttpException(400, "required view doesn't exist");
		}
	}

	/**
	* This method is called each time before an action will be run.
	* It can be used to do some pre checks that can be done for all actions
	*/
	public function beforeAction() {

	}

	/**
	* This method is called each time after an action will be run.
	* It can be used to do some checks that can be done for all actions
	*/
	public function afterAction() {

	}
}