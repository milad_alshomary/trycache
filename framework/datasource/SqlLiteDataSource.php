<?php
namespace framework\datasource;

use framework\datasource\DataSource;
/**
 * class SqlLiteDataSource
 * @author Milad Alshomary
 * SqlLiteDataSource class to implement DataSource behvior in SqlLite way
 */
class SqlLiteDataSource implements DataSource {

	private static $connection;

	public function __construct($config) {
		$this->connection = new \SQLite3($config['database']);
	}

	/**
	* @var Model the model to be inserted into the datasource
	* @return boolean
	*/
	public function insert($model){
		//prepare the connection statment and bind values to it
		$attributes = $model->getAttributes();
		$table_name = $model->sourceName();
		$fields     = implode(',', array_keys($attributes));
		$values     = implode(',:', array_keys($attributes));
		$statment   = $this->connection->prepare("INSERT INTO $table_name ($fields) VALUES (:$values);");
		foreach ($attributes as $key => $value) {
			$statment->bindValue(':' . $key, $value);
		}

		$result = $statment->execute();
		return $result;
	}

	/**
	* @var id identifier of the model
	* @var Model the model to be inserted into the datasource
	* @return boolean
	*/
	public function update($id, $model){
		//prepare the connection statment and bind values to it
		$attributes = $model->getAttributes();
		$table_name = $model->sourceName();
		$updates = array_map(function($key){return $key . '=:' . $key;}, array_keys($attributes));
		$updates    = implode(',', $updates);
		$statement  = $this->connection->prepare("UPDATE $table_name SET $updates WHERE id=:id;");
		$statement->bindValue(':id', $id);
		foreach ($attributes as $key => $value) {
			$statement->bindValue(':' . $key, $value);
		}
		$result = $statement->execute();
		return $result;
	}

	/**
	* @var $criteria array of key and value to match
	* @var $model_class model class associated with the find method
	* @return array of results
	*/
	public function find($model_type, $criteria=[]){
		$table_name = $model_type::sourceName();
		$statement  = $this->connection->prepare("SELECT * FROM $table_name");
		if($criteria != []){
			$condition = array_map(function($key){return $key . '=:' . $key;}, array_keys($criteria));
			$condition = implode(' AND ', $condition);
			$statement  = $this->connection->prepare("SELECT * FROM $table_name WHERE $condition");
		}

		foreach ($criteria as $key => $value) {
			$statement->bindValue(':' . $key, $value);
		}

		$result = $statement->execute();
		$models = [];
		while($row = $result->fetchArray(\SQLITE3_ASSOC)){
			$model = new $model_type; 
			$model->setAttributes($row);
			$models[] = $model;
		}

		return $models;
	}

	/**
	* @var id unique identifier of the model
	* @return Model or null
	*/
	public function findOne($model_type, $id){
		$table_name = $model_type::sourceName();
		//TODO make sure the table has primary key and its name is $id
		$statement  = $this->connection->prepare("SELECT * FROM $table_name WHERE id=:id limit 1");
		$statement->bindValue(':id', $id);
		$result = $statement->execute();
		
		$model = null;
		while($row = $result->fetchArray(\SQLITE3_ASSOC)){
			$model = new $model_type; 
			$model->setAttributes($row);
		}

		return $model;
	}

	/**
	* @var id unique identifier of the model
	* @return true or false
	*/
	public function deleteBy($key, $model) {
		$table_name = $model->sourceName();
		//TODO make sure the table has primary key and its name is $id
		$statement  = $this->connection->prepare("DELETE FROM $table_name WHERE $key=:key");
		$statement->bindValue(':key', $model->$key);
		$result = $statement->execute();
		return $result;
	}

	/**
	* get last inserted id 
	*/
	public function getLastInsertedId() {
		return $this->connection->lastInsertRowID();
	}
}