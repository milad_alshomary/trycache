<?php
namespace framework\datasource;

/**
 * interface DataSource
 * @author Milad Alshomary
 * DataSource interface to define all the method required to be provided 
 * by any datasource(csv, mysql .. etc)
 */
interface DataSource {

	/**
	* @var Model the model to be inserted into the datasource
	* @return boolean
	*/
	public function insert($model);

	/**
	* @var id identifier of the model (in csv maybe its the line number, 
	* in mysql it can be the primarykey)
	* @var Model the model to be inserted into the datasource
	* @return boolean
	*/
	public function update($id, $model);

	/**
	* @var criteria array of key and value to match
	* @var model_type string class name to be initialized
	* @return array of results
	*/
	public function find($model_type, $criteria);

	/**
	* @var id unique identifier of the model
	* @var model_type string class name to be initialized
	* @return Model or null
	*/
	public function findOne($model_type, $id);


	/**
	* get last inserted id to the datasrouce
	*/
	public function getLastInsertedId();
}