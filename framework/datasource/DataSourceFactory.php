<?php
namespace framework\datasource;

use framework\datasource\SqlLiteDataSource;
/**
 * Class DataSourceFactory
 * @author Milad Alshomary
 * DataSourceFactory class is a factory class that handles the initializing of datasource
 */
class DataSourceFactory {

	/**
	* @var $config array of configuration to tell the
	* factory what kind of data source we need to deal with
	*/
	public static function getDataSource($config) {
		$type = isset($config['type']) ? $config['type'] : 'csv';
		switch ($type) {
			case 'sqlite':
				return new SqlLiteDatasource($config);
				break;
				case 'csv' :
				//TODO implement the csv datasource
				break;
		}
	}
}