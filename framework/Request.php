<?php
namespace framework;

class Request {

	public $path;
	
	private $get_params;
	
	private $post_params;
	
	public $host_name;
	
	public $http_method;

	public $app_path;

	/**
	* Constructor.
	* Parsing $_SERVER['url'] into Request object
	*/
	public function __construct(){
		$this->path = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
		$this->host_name   = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
		$this->get_params  = $_GET;
		$this->post_params = $_POST;
		$this->http_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
		$this->app_path    = isset($_SERVER['DOCUMENT_ROOT']) ?  $_SERVER['DOCUMENT_ROOT'] : '';
	}

	/**
	* @var $param String the param to be retrieved from request get array
	* @var $default the value to be returned if the param doesn't exist
	*/
	public function getParam($param, $default){
		if(isset($this->get_params[$param])){
			return $this->get_params[$param];
		} else {
			return $default;
		}
	}

	/**
	* @var $param String the param to be retrieved from request post array
	* @var $default the value to be returned if the param doesn't exist
	*/
	public function postParam($param, $default){
		if(isset($this->post_params[$param])){
			return $this->post_params[$param];
		} else {
			return $default;
		}
	}
}