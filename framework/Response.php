<?php
namespace framework;

use framework\HttpException;

/**
 * Class Response
 * @author Milad Alshomary
 * Response class is used to handle response rendering logic. Like
 * showing json or html and headers etc..
 */
class Response {

	/**
	* @var String, response type json or html
	*/	
	private $response_type;

	public function __construct($response_type){
		$this->response_type = $response_type;
	}

	/**
	* rendering content
	*/
	public function render($content){
		$this->hanldeHeaders();
		$this->renderContent($content);
	}

	public function renderException($ex) {
		switch ($this->response_type) {
			case 'html':
				//TODO rendering customized html to render the exception
				echo 'Exception : ' . $ex->getMessage();
				break;
			case 'json' :
				$this->renderJson(['status' => $ex->status, 'message' => $ex->message]);
				break;
			default:
				echo 'Exception : ' . $ex->getMessage();
				break;
		}
	}

	private function renderContent($content) {
		switch ($this->response_type) {
			case 'html':
				$this->renderHtml($content);
				break;
			case 'json' :
				$this->renderJson($content);
				break;
			default:
				$this->renderHtml($content);
				break;
		}
	}

	private function hanldeHeaders() {
		switch ($this->response_type) {
			case 'html':
				header('Content-Type: text/html');
				break;
			case 'json' :
				header('Content-Type: application/json');
		}
	}

	/**
	* render the content as html
	*/
	private function renderHtml($content) {
		//for now just render the html
		if(is_array($content)) {
			throw new HttpException(400, "not supported response");
		} else {
			echo $content;
		}
	}

	/**
	* render the content as json
	*/
	private function renderJson($content) {
		if(is_array($content)) {
			echo json_encode($content);
		} else {
			echo json_encode([
				'status'  => 200,
				'content' => $content
			]);
		}
	}
}