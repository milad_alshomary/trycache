<?php
use framework\datasource\DataSourceFactory;
use application\models\Address;

class modelTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        //Loading the framework to be tested
        if(!class_exists('AutoLoader')){
            require(dirname(__file__).'/../../../AutoLoader.php');
            AutoLoader::register();
        }
    }

    protected function _after() {
    }

    // tests
    public function testSqlLiteDataSource() {
        $datasource = DataSourceFactory::getDatasource(['type'=>'sqlite', 'database'=>'testingdb']);
        //find model with -1 id and the result will be null
        $model = $datasource->findOne(Address::class, -1);
        $this->assertTrue($model == null);

        //find model with correct id and the result would be model of Address type
        $model = $datasource->findOne(Address::class, 1);
        $this->assertTrue($model =! null);
        
        $model = $datasource->findOne(Address::class, 1);
        $this->assertTrue($model->id == 1);

        //test inserting
        $model = new Address();
        $model->phone = 0786291690;
        $model->street= 'amman jordan';
        $model->name = 'milad alshomary';
        $datasource->insert($model);
        $last_id = $datasource->getLastInsertedId();

        //remove the last inserted record
        $datasource->deleteBy('name', $model);        
    }


    public function testFrameWorkApplication() {
        //initialize the application 
        \framework\Application::init(['response_type'=>'json','datasource' => ['type' => 'sqlite','database' => 'testingdb'],'routes' => ['GET /users' => 'user/list']]);

        $model = new Address();
        $model->phone = 0786291690;
        $model->street= 'amman jordan';
        $model->name = 'milad alshomary';
        $model->save();
        $this->assertTrue(is_integer($model->id));
    }
}