<?php
//to auto load all the classes in the framework and appliation folder
include "../AutoLoader.php";
AutoLoader::register();

//loading configuration from ./config.php file
$config = require('./config.php');

//initializing the application and run it
\framework\Application::init($config);
//run the instance
\framework\Application::getInstance()->run();