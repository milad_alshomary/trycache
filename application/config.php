<?php
return [
	'response_type' => 'json',
	'datasource' => [
		'type'   => 'sqlite',
		'database' => 'database/localdb'
	],
	'routes' => [
		'GET /address' => 'address/actionGet',
		'GET /addresses' => 'address/actionList',
		'POST /address' => 'address/actionCreate',
		'DELETE /address' => 'address/actionDelete',
		'PUT /address' => 'address/actionUpdate',
		'GET /welcome' => 'address/actionWelcomPage',
	]
];