<?php
namespace application\controllers;

use framework\Controller;
use application\models\Address;
use framework\Application;
use framework\HttpException;

class AddressController extends Controller{

	/**
	* @return String html. this function
	* is to demonstrate the way to render on of the html views
	*/
	public function actionWelcomPage() {
		return $this->fetchView('welcome');
	}

	/**
	* @return Address, model address
	*/	
	public function actionGet() {
		$id = Application::getInstance()->request->getParam('id',null);
		if($id == null) {
			throw new HttpException(404, "address doesn't exist");
		} else {
			$model = Address::findOne($id);
			if($model == null) {
				throw new HttpException(404, "address doesn't exist");
			} else{
				return $model;
			}
		}
	}

	/**
	* @return array, list of addresses
	*/	
	public function actionlist() {
		//retrieve all addresses from datasource.
		//TODO support pagination through page,limit
		$models = Address::find();
		return $models;
	}

	/**
	* @return boolean, create address from the 
	* post request parameters
	*/	
	public function actionCreate() {
		$phone = Application::getInstance()->request->postParam('phone', null);
		$name  = Application::getInstance()->request->postParam('name', null);
		$street= Application::getInstance()->request->postParam('street', null);

		$model = new Address();
		$model->setAttributes([
			'phone' => $phone,
			'name' => $name,
			'street' => $street,
		]);

		if($model->save()){
			return [
					"status" => 200
				];
		} else {
			return [
					"status" => 400,
					"msg" => "error in adding the address"
				];
		}
	}

	/**
	* @return boolean, delete address specified by id
	*/	
	public function actionDelete() {
		$id = Application::getInstance()->request->getParam('id',null);
		$model = Address::findOne($id);
		if($model == null) {
			throw new HttpException(404, "address doesn't exist");
		} else{
			if(!$model->removeBy('id')){
				throw new HttpException(400, "error in deleting address");
			} else {
				return [
					"status" => 200,
					"msg" => "model is deleted"
				];
			}
		}
	}

	/**
	* @return boolean, update address specified by id
	*/	
	public function actionUpdate() {
		$id = Application::getInstance()->request->getParam('id',null);
		$model = Address::findOne($id);

		if($model == null) {
			throw new HttpException(404, "address doesn't exist");
		} else{
			$phone = Application::getInstance()->request->postParam('phone', null);
			$name  = Application::getInstance()->request->postParam('name', null);
			$street= Application::getInstance()->request->postParam('street', null);

			$model->setAttributes([
				'phone' => $phone,
				'name' => $name,
				'street' => $street
			]);

			if(!$model->save()){
				throw new HttpException(400, "error in updating address");
			} else {
				return [
					"status" => 200,
					"msg" => "model is updated"
				];
			}
		}
	}

}