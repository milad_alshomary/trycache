<?php
namespace application\models;

use framework\Model;

class Address extends Model {
	
	public static function sourceName(){
		return 'address';
	}

	/**
	* override Model::validateModel to do some validation on
	* the attributes
	*/
	public function validateModel() {
		if(strlen($this->name) > 50) {
			return false;
		}

		return true;
	}
	
}